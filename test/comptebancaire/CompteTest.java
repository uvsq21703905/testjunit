package comptebancaire;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CompteTest {

    @Test
    public void testCredit(){
        Compte c = new Compte(20);
        c.crediter(10);
        assertEquals(30,c.getSolde());
    }

    @Test
    public void testDebit(){
        Compte c = new Compte(20);
        c.debiter(10);
        assertEquals(10,c.getSolde());
    }

    @Test
    public void testVirement(){
        Compte c1 = new Compte(20);
        Compte c2 = new Compte(40);
        c2.virer(10,c1);
        assertEquals(30,c1.getSolde());
        assertEquals(30,c2.getSolde());
    }
}
