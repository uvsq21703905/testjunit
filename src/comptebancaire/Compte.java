package comptebancaire;

public class Compte {

    private int solde;

    public Compte(int montant) {
        if( montant >= 0) this.solde = montant;
        else              this.solde = 0;
    }

    public void crediter(int montant) {
        if( montant > 0) this.solde = this.solde + montant;
    }

    public int getSolde(){
        return this.solde;
    }

    public void debiter(int montant){
        if( montant >= 0 && this.solde - montant >= 0) this.solde = this.solde - montant;
    }

    public void virer(int montant, Compte c){
            this.debiter(montant);
            c.crediter(montant);
        }
}
